
<?php
 //Single
echo 'Love Yourself';

?>
<br>

<?php 
 // Double
    $s = 'euros';

    echo "This costs a lot of $s";
// echo This costs a lot of Euros;

?>
<br>

<?php
  //Heredoc
class baseline
{
    var $baseline;
    var $bar;
    

    function __construct()
    {
        $this->baseline = 'Larry';
        $this->bar = array('Salute', 'Hope', 'Unity');
    }
}

$baseline = new baseline();
$name = 'Denny';

echo <<<EOT
My name is "$name". I am printing some $baseline->baseline.
Now, I am printing some {$bar->bar[1]}.
This should print a capital 'A': \x40;
EOT;
?>

<br>

<?php 
//Nowdoc

class puppy
{
    public $puppy;
    public $bar;

function __construct()
    {
        $this->puppy = 'dog';
        $this->bar = array('Salute', 'Hope', 'Unity');
    }
}

$puppy = new puppy();
$name = 'MyName';

echo <<<'EOT'
My name is "$name". I am printing some $puppy->puppy.
Now, I am printing some {$puppy->bar[1]}.
This should not print a capital 'A': \x40
EOT;
?>