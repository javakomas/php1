Įvadas. Pasiruošimas darbui
Tikslas

Pasiruošti PHP programavimo aplinką - pasiruošti teksto redaktorių, PHP serverį ir 
Github/Gitlab nustatymus

https://drive.google.com/open?id=1tj5awMPRh91q7dep9VOEraJ_-XNwywFpOCe718xKon8
Teksto redaktorius

Kodo rašymui teksto redaktorių galite naudoti, kokį norite - NetBeans, VSCode ar
 koks kitas galingesnis teksto redaktorius.

Rekomenduojamas Visual Studio Code - https://code.visualstudio.com/download

Teksto redaktorius sugeba automatiškai išlygiuoti papildomsu tarpus ir atskyrimus nuo krašto.
 Įsirašykite plugin'ą phpfmt 


PHP web serveris

Web serveris naudojamas JS, CSS, pavekslėliams, .php ir kitiems failams tiekti svetainėje.

Ant Windows'ų naudosime XAMPP development aplinką, kuri pakuria serverį lokaliai jūsų kompiuteryje -
 https://www.apachefriends.org/download.html (pasirinkti PHP 7.2+)

Jeigu neveikia XAMPP apache serveris, galite pabandyti laikinai išjungti skype.

PHP leidimas iš komandinės eilutės

Jeigu PHP neveikia iš komandinės eilutės "php is not recognized as internal or external command...", 


Git
Rašomam kodui naudosime kodo versijavimo įrankį git tam, kad kodas ir 
jo skirtingos versijos būtų išsaugotos ir neprarastos.
Būtina naudoti. Būtina visą kodą, kurį rašome susikelti į public git repozitoriją.

Rekomenduoju kiekvienai paskaitai ar atskirai užduočiai susikurti atskirą git repozitoriją.

Jeigu neturite suskonfigūravę savo Gitlab ar Github paskyros, susikurkite: 

    Susikurti gitlab paskyrą https://gitlab.com/users/sign_in
    Pridėti savo ssh key į gitlab https://docs.gitlab.com/ee/ssh/

Git naudosime per teksto redaktoriaus plugin'ą.

Jeigu neveikia git, pabandykite įsirašyti git for windows:

Bonus

Neprivaloma.

Tiems, kurie naudoja Windows, rekomenduoju pasikurti Ubuntu(Linux) operacinę sistemą šalia Windows tam, 
kad išmokti geriau naudotis Linux komandine eilute ir išmokti Linux serverio administravimo.

Pasidarykite kompiuterio failų kopiją ir šalia švarios windows instaliacijos įsirašykite ubuntu:
https://www.tecmint.com/install-ubuntu-16-04-alongside-with-windows-10-or-8-in-dual-boot/
