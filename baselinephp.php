PHP-1 - Sintaksė
Tikslas

    Sužinoti kas yra ir kaip veikia PHP serveris ir PHP interpretatorius.
    Parašyti pirmą programą PHP kalboje vykdymui per komandinę eilutę
    Parašyti pirmą programą PHP kalboje vykdymui per PHP web serverį
    Parašyti programą, kuri naudoja PHP žymes įterptas į HTML
    Parašyti programą, kuri apdoroja web formą PHP serveryje
    Apžvelgti PHP kodo sintaksės pagrindus


PHP interpretatorius

    Turi savo versiją (7.2)
    Perskaito PHP skriptą ir spausdina rezultatą


Failai

PHP skripto failai privalo turėti ".php" plėtinį. Failas turi pirmoje eilutėje turėti kodą:

<?php

tuomet rašomas kodas iš naujos eilutės. Uždarymo tagas "?>" nenaudojamas.


Komandinės eilutės komandos

php -v   	parodo php versiją. Naudojama patikrinti,
     ar php sukonfigūruotas paleidimui per komandinę eilutę 
php -info   	parodo php konfigūraciją
php -S localhost:8000   	pakuria php built-in web serverį (galima naudoti vietoj xampp web serverio) 


Konfigūracijos patikrinimas
PHP skriptas patikrinti kokia php versija naudojama web serveryje:

<?php
phpinfo();


Pirma programa

index.php:

<?php

echo 'Labas, pasauli';

Programos leidimas per komandinę eilutę

php index.php


Programa įterpiant PHP į HTML

index.php:

<html>
<head></head>
<body>
<p>The time is: <?php echo date('Y-m-d H:i:s'); ?>. </p>
</body>


Programa apdorojanti formos duomenis serveryje

https://gitlab.com/Rusted/php-1/blob/master/form.php

Apie formas daugiau mokinsimės vėlesnėse paskaitose.

Kodo stilius

Kodo stiliaus taisyklės neturi įtakos programos efektyvumui, bet stipriai padeda bendrai tvarkai,
 kodo skaitomumui ir darbui komandoje.

    Kintamieji ir funkcijų pavadinimai rašomi naudojant camelCase stilių 
    https://en.wikipedia.org/wiki/Camel_case. 
    Klasių pavadinimai turi prasidėti iš didžiosios, kintamųjų ir funkcijų - iš mažosios.
    Būtina naudoti anglų kalbą rašant kodą. 
    Vengti naudoti sutrumpintus pavadinimus, nebijoti rašyti ilgesnį pavadinimą iš kelių pilnų žodžių.
    po užsidarančio skliausto "}" palikti tuščią eilutę
    galima dėl skaitomumo dėti paildomai tuščių eilučių - naudoti saikingai

    $item, $payment, $userAvatar, $customerPaymentReportSheet - kintamųjų pvz.
    getUserAvatar(), getPayment(), export() - funkcijų pvz.
    User, UserAvatar - klasių pvz.

    Programos lygiavimui naudojami 4 tarpo simboliai vietoj tab (nusistatyti teksto redaktoriuje).
    Apsirašant funkciją ar klasę,
     atsidarantis ir užsidarantis kliaustas rašomas iš naujos eilutės (mokysimės vėliau), pvz.:

function sayHello(name)
{
    echo "Hello, $name";
}

Daugiau info apie kodo stilių - peržvelgti:

    https://www.php-fig.org/psr/psr-1/
    https://www.php-fig.org/psr/psr-2/
    https://symfony.com/doc/current/contributing/code/standards.html#structure
    https://github.com/jupeter/clean-code-php


Kintamųjų tipai

bool 	$agreedToTermsAndConditions = true;
integer    	$count = 3; $score = -100; $itemsInBasket = 0;
float or double  	$price = 99.99; $balance = -10.01;
string 	$name = 'John'; $address= 'Žirmūnų 1';
null 	$currentUser = null;
array 	$fruits = ['apple', 'orange'];
object 	$unknownException = new \Exception('Something went wrong');
resource 	$handle = fopen("c:\\folder\\resource.txt", "r");
 $dbHandle = new PDO('mysql:host=localhost;dbname=test', $user, $pass);

Resource tipas yra nuoroda į išorinį resursą, kurį įsimena php variklis.
 Dažnai naudojama kaip nuoroda į failą diske arba kaip nuoroda į db connection'ą
Daugiau info apie PHP tipus: http://php.net/manual/en/language.types.intro.php 

Aritmetiniai Operatoriai
Pvz   	Veiksmas   	Panaudojimas  
$a++ 	Pridėti 1 	
$a-- 	Atimti 1 	
$a + $b 	Sudėtis 	
$a - $b 	Atimtis 	
$a * $b 	Daugyba 	
$a / $b 	Dalyba 	
$a % $b 	Liekana 	Galima naudoti skaičių išskirstymui - lyginis/nelyginis, gauti kas 3 skaičių ir
 pan.
$a ** $b 	Kėlimas laipsniu 	

Taip pat galima naudoti priskyrimo operatorių su bet kuria operacija, pvz. lygiavertės operacijos:
$a-=3;
$a = a - 3;

Daugiau info: http://php.net/manual/en/language.operators.arithmetic.php,
 http://php.net/manual/en/language.operators.assignment.php.

Dėmesio, atliekant aritmetinę operaciją string tipo kintamieji paverčiami į skaičių,
 jeigu neįmanoma - į 0.
'+' operacija nesujungia string kintamųjų,
 o juos paverčia skaičiais ir bando sudėti, pvz. 'Laba ' + 'diena' = 0;

String operatoriai

$a . $b    	sujungimas 	
$a .= ' thank you for reading'    	sujungimas ir priskyrimas  
	tas pats kas: $a = $a . ' thank you for reading'

